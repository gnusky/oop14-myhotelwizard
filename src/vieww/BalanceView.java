package vieww;

import java.awt.BorderLayout;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Map.Entry;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import controller.Controller;
import model.Hotel;
import model.Pair;

public class BalanceView extends JFrame implements Serializable{


	private static final long serialVersionUID = -8521810039861325270L;

	String[] nameColumns = { "Voce", "Importo" };

	private JButton addCost;
	private JButton addReturn;
	private JButton removeCost;
	private JButton removeReturn;
	private JTable attivo;
	private JTable passivo;
	DefaultTableModel model;
	DefaultTableModel passiveModel;
	private JPanel rightPanel = new JPanel();
	private JPanel leftPanel = new JPanel();
	private JPanel bottomPanel = new JPanel();
	private String[] blankRow = { "", "" };

	BalanceView() {
		
		this.initialize();
		this.setSize(500, 500);
		this.getContentPane().setLayout(new BorderLayout());
		attivo = new JTable(model);
		passivo = new JTable(passiveModel);
		JScrollPane active = new JScrollPane(attivo);
		JScrollPane passive = new JScrollPane(passivo);
		rightPanel.setBorder(new TitledBorder("Ricavi"));
		leftPanel.setBorder(new TitledBorder("Costi"));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.PAGE_AXIS));
		rightPanel.add(active);
		leftPanel.add(passive);
		this.add(leftPanel, BorderLayout.WEST);
		this.add(rightPanel, BorderLayout.EAST);
		addCost = new JButton("Aggiungi voce di costo");
		addReturn = new JButton("Aggiungi voce di ricavo");
		removeCost = new JButton("Rimuovere voce di costo");
		removeReturn = new JButton("Rimuovere voce di ricavo");
		rightPanel.add(addReturn);
		rightPanel.add(removeReturn);
		leftPanel.add(addCost);
		leftPanel.add(removeCost);
		this.add(bottomPanel, BorderLayout.SOUTH);
		model.addRow(blankRow);
		passiveModel.addRow(blankRow);
		addCost.addActionListener(e -> {
			if (passiveModel.getRowCount() > 0) {
				if (((String) passiveModel.getValueAt(passiveModel.getRowCount() - 1, 2)).contains(".")) {
					Object[] tmp = { passiveModel.getValueAt(passiveModel.getRowCount() - 1, 0),
							passiveModel.getValueAt(passiveModel.getRowCount() - 1, 1),
							passiveModel.getValueAt(passiveModel.getRowCount() - 1, 2) };
					Controller.getInstance().addBalance(tmp, false);
				} else {
					String sb = ((String) passiveModel.getValueAt(passiveModel.getRowCount() - 1, 2)) + ".";
					Object[] tmp = { passiveModel.getValueAt(passiveModel.getRowCount() - 1, 0),
							passiveModel.getValueAt(passiveModel.getRowCount() - 1, 1), sb };
					Controller.getInstance().addBalance(tmp, false);
				}
				passiveModel.addRow(blankRow);
				this.initialize();
				this.revalidate();
				this.repaint();
			}

		});
		addReturn.addActionListener(e -> {
			if (model.getRowCount() > 0) {
				Object[] tmp = { model.getValueAt(model.getRowCount() - 1, 0),
						model.getValueAt(model.getRowCount() - 1, 1), model.getValueAt(model.getRowCount() - 1, 2) };
				if (!((String) model.getValueAt(model.getRowCount() - 1, 2)).contains("\\.")) {
					String tmp2 = (String) (model.getValueAt(model.getRowCount() - 1, 2) + "\\.0");
					tmp[2] = tmp2;
				}
				Controller.getInstance().addBalance(tmp, true);
				model.addRow(blankRow);
				this.initialize();
				this.revalidate();
				this.repaint();
			}

		});
		removeCost.addActionListener(e -> {
			if (passiveModel.getRowCount() > 0) {
				int row = passivo.getSelectedRow();
				Object data = (Object) passivo.getValueAt(row, 0);
				int counter = 0;
				for (int i = 0; i < passiveModel.getRowCount(); i++) {
					if (i != row) {
						if (data.equals(passivo.getValueAt(i, 0))) {
							counter++;
						}
					}
				}
				Controller.getInstance().removeRow((String) data, counter, false);
				passiveModel.removeRow(passivo.getSelectedRow());
				this.initialize();
				this.revalidate();
				this.repaint();
			}
		});
		removeReturn.addActionListener(e -> {
			if (model.getRowCount() > 0) {
				int row = passivo.getSelectedRow();
				Object data = (Object) attivo.getValueAt(row, 0);
				int counter = 0;
				for (int i = 0; i < model.getRowCount(); i++) {
					if (i != row) {
						if (data.equals(attivo.getValueAt(i, 0))) {
							counter++;
						}
					}
				}
				Controller.getInstance().removeRow((String) data, counter, true);
				model.removeRow(attivo.getSelectedRow());
				this.initialize();
				this.revalidate();
				this.repaint();
			}
		});
		this.setVisible(true);
		this.pack();
	}

	public void initialize() {
		model = new DefaultTableModel(new Object[][] {}, new Object[] { "Data", "Voce", "Importo" });
		if (!Hotel.getInstance().getIncoming().values().isEmpty()) {
			for (Entry<LocalDate, List<Pair<String, Double>>> et : Hotel.getInstance().getIncoming().entrySet()) {
				for (Pair<String, Double> pair : et.getValue()) {
					String[] toAdd = { Controller.fromLocalDateToString(et.getKey()), pair.getKey(),
							pair.getValue().toString() };
					model.addRow(toAdd);
				}
			}
		}
		passiveModel = new DefaultTableModel(new Object[][] {}, new Object[] { "Data", "Voce", "Importo" });
		if (!Hotel.getInstance().getOutcoming().values().isEmpty()) {
			passiveModel = new DefaultTableModel(new Object[][] {}, new Object[] { "Data", "Voce", "Importo" });
			for (Entry<LocalDate, List<Pair<String, Double>>> et : Hotel.getInstance().getOutcoming().entrySet()) {
				for (Pair<String, Double> pair : et.getValue()) {
					String[] toAdd = { Controller.fromLocalDateToString(et.getKey()), pair.getKey(),
							pair.getValue().toString() };
					passiveModel.addRow(toAdd);
				}
			}
		}
	}
}