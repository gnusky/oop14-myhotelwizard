package vieww;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class StartMenu extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JButton createHotel = new JButton("Crea nuovo Hotel");
	private final JButton manageRoom = new JButton("Visualizza camere");
	private final JButton manageHotel = new JButton("Gestione Hotel");
	

	StartMenu() {
		// this.setPreferredSize(new Dimension(100,100));
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		createHotel.setAlignmentX(CENTER_ALIGNMENT);
		this.add(createHotel);
		this.add(manageRoom);
		this.add(manageHotel);

		this.setVisible(true);
		// this.setOpaque(true);
	}

}