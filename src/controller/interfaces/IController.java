package controller.interfaces;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Booking;
import model.Customer;
import model.Room;
import model.RoomType;

public interface IController {
	/**
	 * Metodo che controlla se la stanza associata al customer passato in
	 * ingresso è occupata in una determinata data.
	 * 
	 * @param date
	 *            la data per la quale controllare se la stanza è occupata
	 * @param customer
	 *            il customer associato a quella determinata stanza
	 * @return true if room is busy, false if room is free
	 */

	public boolean checkIfRoomIsBusy(final LocalDate date, final Customer customer);
	

	/**
	 * Metodo che controlla se la stanza associata ad un customer passato in
	 * ingresso è occupata per un determinato intervallo di tempo.
	 * 
	 * @param start
	 *            la data di inizio per il checking sulla room
	 * @param end
	 *            la data di fine per il checking sulla room
	 * @param customer
	 *            il customer associato alla room da controllare
	 * @return true if room is busy for even one day of the period, false
	 *         altrimenti
	 */

	public boolean checkIfRoomIsBusyPeriod(final LocalDate start, final LocalDate end, final Customer customer);
	
	/**
	 * Metodo che ritorna lo stato attuale di tutte le stanze in una determinata
	 * data.
	 * 
	 * @param date
	 *            la data sulla quale eseguire il chec.k
	 * @param rooms
	 *            la lista di stanze dell'hotel
	 * @return myMap la mappa con le stanze dell'hotel mappate con valori
	 *         boolean ad indicare se la room è busy o meno in quel determinato
	 *         giorno.
	 */


	public Map<Room, Boolean> roomsStatus(LocalDate date, List<Room> rooms);

	/**
	 * Metodo che prende in ingresso un determinato intervallo di tempo, un
	 * numero che rappresenta i guest che andranno a comporre un determinato
	 * booking e che esegue il controllo sulle stanze disponibili in quel
	 * determinato periodo di tempo con un numero massimo di ospiti >= del
	 * parametro passato in ingresso.
	 * 
	 * @param start
	 *            data di inizio del soggiorno
	 * @param end
	 *            data di fine del soggiorno
	 * @param guestsNumber
	 *            numero di guest del tentativo di booking
	 * @return avaliableRooms le stanze disponibili
	 * 
	 * 
	 */

	public Set<Room> getBookingResults(final LocalDate start, final LocalDate end, final int guestsNumber);
	
	/**
	 * Metodo che prende in ingresso un determinato intervallo di tempo, un
	 * numero che rappresenta i guest che andranno a comporre un determinato
	 * booking e che esegue il controllo sulle stanze disponibili in quel
	 * determinato periodo di tempo con un numero massimo di ospiti >= del
	 * parametro passato in ingresso e un RoomType che andrà a fungere da filtro
	 * aggiuntivo sul checking delle stanze
	 * 
	 * @param start
	 *            data di inizio del soggiorno
	 * @param end
	 *            data di fine del soggiorno
	 * @param guestsNumber
	 *            numero di guest del tentativo di booking
	 * 
	 * @param rt
	 *            il tipo di stanza richiesta dal prenotante
	 * @return avaliableRooms le stanze disponibili
	 * 
	 * 
	 */

	public Set<Room> getFilteredBookingResults(final LocalDate start, final LocalDate end, final int guestsNumber,
			final RoomType rt);
	

	/**
	 * Metodo che prende in ingresso un intero(numero di stanza) e ritorna un
	 * boolean in base alla presenza o meno di una stanza associata a quel
	 * numero all'interno dell'istanza di Hotel
	 * 
	 * @param roomValue
	 *            l'intero della stanza da cercare
	 * @return true if roomnumber is present, false altrimenti
	 * 
	 */

	public boolean checkIfRoomIsPresent(final int roomValue);

	public void saveCatalog(final double highPercentage, final double midPercentage, final double day,
			final double BBOverPrice, final double halfBoardOverPrice, final double fullBoardOverPrice,
			final double premiumOverPrice, final double suiteOverPrice, final double childPercentage,
			final int childAge, final int babyAge);

	public void addBalance(final Object[] values, final boolean isIncoming);

	public void removeRow(final String string, final int index, final boolean isIncoming);
	
	/**
	 * Metodo che collega il customer, il booking e la stanza passati in
	 * ingresso dalla view istanziandoli nel model.
	 * 
	 * @param c
	 *            customer(name,surname,cf,birth)
	 * @param b
	 *            booking associato al customer
	 * @param r
	 *            room associata al booking
	 * 
	 */

	public void createBooking(final Customer c, final Booking b, final Room r);
	
	/**
	 * Metodo che aggiunge un extra passato in ingresso ad un booking passato in
	 * ingresso
	 * 
	 * @param extra
	 * @param booking
	 */

	public void addExtraToBooking(final Booking b, final String extra);
	
	/**
	 * Metodo che viene fatto runnare nel main per settare ad ogni avvio gli
	 * actualcustomers di stanze che presentano una prenotazione, esegue il
	 * check sulla data odierna e se vi è un customer nella customerlist della
	 * room che ha un booking con start = LocalDate.now() setta per quella room
	 * quello specifico customer come actualCustomer della room
	 * 
	 */

	public void computeBusyRooms();
	

	/**
	 * Metodo che prende in ingresso due stringe necessarie all'aggiunta di un
	 * piatto all'interno del dailyMenu dell'hotel
	 * 
	 * @param type
	 *            la stringa tipo di piatto(antipasti, primi,secondi..)
	 * @param text
	 *            la stringa di testo che denomina il nome del piatto da
	 *            aggiungere nel menu
	 * 
	 */

	public void addMenuVoice(final String type, final String text);
}