/*
 * Enum che modella i 4 tipi di pensione disponibili per il nostro hotel.
 */
package model;

import java.io.Serializable;

public enum BookingType implements Serializable {
	OVERNIGHT, BB, HALFBOARD, FULLBOARD;
}
