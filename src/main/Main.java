package main;

import controller.Controller;
import model.Hotel;
import vieww.MainFrame;

public class Main {

	public static void main(String[] args) {
		Hotel model = Hotel.getInstance();
		Hotel.saveInstance();
		MainFrame view = MainFrame.getInstance();
		view.updateButtons();
		Controller controller = Controller.getInstance();
		Controller.getInstance().computeBusyRooms();
	}
}